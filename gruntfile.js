'use strict';

module.exports = function(grunt) {
    var path = require('path');

    // Unified Watch Object
    var watchFiles = {
        serverJS: ['gruntfile.js'],
        mochaTests: ['app/tests/**/*.js']
    };

    // Project Configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        wait: {
            run_store_register: {
                options: {
                    delay: 15000,
                    after: function() {
                        grunt.task.run('test_all');
                    }
                }
            },
            run_test: {
                options: {
                    delay: 2000,
                    after: function() {
                        grunt.task.run(['protractor:login', 'protractor:global']);
                    }
                }
            }
        },
        shell: {
            protractor: {
                options: {
                    stdout: true
                },
                command: 'node ' + path.resolve('node_modules/protractor/bin/webdriver-manager') + ' update --standalone --chrome'
            },
            backend_test_server: {
                command: 'node server.js'
            }
        },
        protractor_webdriver: {
            alive: {
                options: {
                    keepAlive: true
                }
            },
            dead: {}
        },
        protractor: {
            auth: {
                options: {
                    configFile: 'test/config/authConf.js',
                    args: {
                        params: '<%= pConfig %>'
                    }
                },
                command: 'protractor test/config/authConf.js'
            },
            oneFile: {
                options: {
                    configFile: 'test/config/oneFileConf.js',
                    args: {
                        specs: '<%= testScript %>',
                        params: '<%= pConfig %>'
                    }
                },
                command: 'protractor test/config/oneFileConf.js'
            }
        }
    });

    // Load NPM tasks
    require('load-grunt-tasks')(grunt);

    // Making grunt default to force in order not to break the project.
    grunt.option('force', true);

    grunt.task.registerTask('loadTestScriptConfig', 'Task that loads the test script config in command param --script.', function() {
        var scriptFile = grunt.option('script');
        var arr = [];
        if (scriptFile) {
            arr = scriptFile.split(',');
        }
        for (var i in arr) {
            arr[i] = 'test/script/' + arr[i] + '.js';
        }
        grunt.config.set('testScript', arr);
    });

    // Test Protractor All script task
    grunt.registerTask('test_all', ['loadTestScriptConfig', 'shell:protractor', 'protractor_webdriver:alive', 'protractor:auth']);

    // Test Protractor One Script task
    grunt.registerTask('test_one', ['loadTestScriptConfig', 'shell:protractor', 'protractor_webdriver:alive', 'protractor:oneFile']);
};
