var utils = require('../utils');

exports.config = {
    baseUrl: 'http://staging-talet.ominext.co/talet/',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    framework: 'jasmine2',
    jasmineNodeOpts: {
        // Remove protractor dot reporter to use jasmine-spec-reporter 
        print: function() {},
        // onComplete will be called just before the driver quits.
        onComplete: null,
        // If true, display spec names.
        isVerbose: true,
        // If true, print colors to the terminal.
        showColors: true,
        // If true, include stack traces in failures.
        includeStackTrace: true,
        // Default time to wait in ms before a test fails.
        defaultTimeoutInterval: 1000000
    },
    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            // Prevent warning about dev tools, which breaks some tests, in Windows at least.
            'args': ['--disable-extensions']
        }
    },
    onPrepare: function() {
        var SpecReporter = require('jasmine-spec-reporter');
        // add jasmine spec reporter
        jasmine.getEnv().addReporter(new SpecReporter({
            displayStacktrace: 'all'
        }));

        browser.driver.manage().window().maximize();
        browser.driver.get(browser.baseUrl);
        var deferred = protractor.promise.defer();
        deferred.fulfill();
        return deferred.promise;
    }
};
