'use strict';

/**
 * Hàm ấn ENTER
 *
 */
exports.enter = function() {
    return browser.driver.actions().sendKeys(protractor.Key.ENTER).perform();
};

/**
 * Hàm ấn ESCAPE
 *
 */
exports.escape = function() {
    return browser.driver.actions().sendKeys(protractor.Key.ESCAPE).perform();
};
